//requestAnimationFrame
(function(){
    requestAnimationFrame = window.requestAnimationFrame ||
                                window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame ||
                                window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();

//Define stuff
c = document.getElementById("game");
ctx = c.getContext("2d");

game.width = document.getElementById("game").width;
game.height = document.getElementById("game").height;

player = {
    x: 96,
    y: game.height-64,
    width: 96,
    height: 96,
    speed: 5,
    velX: 0,
    velY: 0,
    jumping: false,
    walking: false,
    spritex: 0,
    spritey: 0,
    facingright: true
};

keys = [];
slide = 0.9;
gravity = 0.5;

controls = {
    up: 87,
    left: 65,
    down: 83,
    right: 68,
    jump: 32
};

function dvorak(){
    controls = {
        up: 188,
        left: 65,
        down: 79,
        right: 69,
        jump: 32
    };
}

//dvorak();

//Images
chicken = new Image();
chicken.src = "GameFiles/Chicken.png";
grass = new Image();
grass.src = "GameFiles/Grass.png";

animspeed = 10;

//Update
function update(){
    if(keys[controls.up]||keys[controls.jump]){
        //Jump
        if(!player.jumping){
            player.jumping = true;
            player.velY = -player.speed*2;
        }
    }
    if(keys[controls.right]){
        //Move right
        if (player.velX < player.speed){
            player.velX++;
        }
        player.facingright = true;
    }          
    if(keys[controls.left]){
        //Move left
        if (player.velX > -player.speed){
            player.velX--;
        }
        player.facingright = false;
    }
    
    player.velX *= slide;
    player.velY += gravity;
    
    if(!player.jumping){
        player.velY = 0;
    }
    
    player.x += player.velX;
    player.y += player.velY;
    
    if(player.x <= 48){
        player.x = 48;
    }
    if(player.y >= game.height-64){
        player.y = game.height-64;
        player.jumping = false;
    }
    if(player.x > game.width/2){
        camerax = player.x-(game.width/2);
    }else{
        camerax = 0;
    }
    if(player.y < game.height/2){
        cameray = player.y-(game.height/2);
    }else{
        cameray = 0;
    }
    //Draw sky
    grd = ctx.createLinearGradient(0, 0, 0, game.height);
    grd.addColorStop(0, "#0040FF");
    grd.addColorStop(1, "#80C0FF");
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, game.width, game.height);
    //Draw grass
    grassx = -(camerax%64);
    if(cameray > -64){
        while(grassx < game.width){
            ctx.drawImage(grass, grassx, (game.height-cameray)-64);
            grassx += 64;
        }
    }
    //Walk Animation Tick
    if(player.walking){
        wat += 1;
    }else{
        wat = 0;
    }
    //Choose Player Sprite
    if(Math.abs(player.velX) > 3){
        player.walking = true;
    }else{
        player.walking = false;
    }
    if((player.walking&&wat%animspeed<animspeed/2)||player.jumping){
        player.spritex = 1;
    }else{
        player.spritex = 0;
    }
    if(player.facingright){
        player.spritey = 0;
    }else{
        player.spritey = 1;
    }
    //Draw Player
    ctx.drawImage(chicken, 96*player.spritex, 96*player.spritey, 96, 96, (player.x-camerax)-48, (player.y-cameray)-96, 96, 96);
    //Draw Stats
    ctx.font = "24px Oxygen";
    ctx.fillStyle = "#00FF00";
    ctx.fillText("camerax = " + Math.round(camerax), 12, 24*1);
    ctx.fillText("cameray = " + Math.round(cameray), 12, 24*2);
    ctx.fillText("player.x = " + Math.round(player.x), 12, 24*3);
    ctx.fillText("player.y = " + Math.round(player.y), 12, 24*4);
    ctx.fillText("player.velX = " + Math.round(player.velX), 12, 24*5);
    ctx.fillText("player.velY = " + Math.round(player.velY), 12, 24*6);
    //Loop
    requestAnimationFrame(update);
}

window.addEventListener("load", update);

//Keypresses
document.body.addEventListener("keydown", function(e) {
    keys[e.keyCode] = true;
});
 
document.body.addEventListener("keyup", function(e) {
    keys[e.keyCode] = false;
});
