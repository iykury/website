function onLoad(){
    loadPreset();
    mixPeoples();
}

function mixPeoples(){
    types = s("#typelist").value.split("\n");
    numPeeps = s("#numPeeps").value;
    peoplesNum = Math.floor(Math.random() * types.length);
    peoplesMix = types[peoplesNum];
    for(i = 1; i < numPeeps; i++){
        peopleNum = Math.floor(Math.random() * types.length);
        peoplesMix += " + " + types[peopleNum];
    }
    s("#mix").innerHTML = peoplesMix;
}
function loadPreset(){
    switch(s("#preset").selectedIndex){
        case 0:
            s("#typelist").value = presets.default.join("\n");
            break;
        case 1:
            s("#typelist").value = presets.monster.join("\n");
    }
}

presets = {
    default: [
        "Cowboy",
        "Pirate",
        "Cyborg",
        "Steampunk",
        "Disco",
        "Villain",
        "Gangster",
        "Police",
        "Zombie",
        "Viking",
        "Magical Girl",
        "Scuba",
        "Gypsy",
        "Medic",
        "Angel",
        "Goth",
        "Wizard",
        "Royalty",
        "Soldier",
        "Ninja",
        "Holyman",
        "Agent",
        "Bodybuilder",
        "Werewolf",
        "Mariachi",
        "Fairy",
        "Hobo",
        "Outdoorsman",
        "Demon",
        "Giant",
        "Superhero",
        "Knight",
        "Astronaut",
        "Gentleman",
        "Chieftain",
        "Scientist",
        "Vampire",
        "Elf",
        "Mermaid",
        "Lumberjack",
        "Cook",
        "Fatty",
        "Mechanic",
        "Lizardman",
        "Baby",
        "Elderly"
    ],
    monster: [
        "Werewolf",
        "Vampire",
        "Cyclops",
        "Siren",
        "Medusa",
        "Scarecrow",
        "Demon",
        "Witch",
        "Skeleton",
        "Slime",
        "Succubus",
        "Zombie",
        "Ghost",
        "Alien",
        "Multi-Eyes",
        "Multi-Limbs",
        "Plant",
        "Insect",
        "Tentacles",
        "Swamp Monster",
        "Doll",
        "Nightmare",
        "Cockatrice",
        "Banshee",
        "Harpy",
        "Ghoul",
        "Wendigo",
        "Dullahan",
        "Mothman",
        "Spider",
        "Yeti",
        "Pumpkin",
        "Chupacabra",
        "Candy",
        "Lamia",
        "Mushroom",
        "Bat"
    ]
}
