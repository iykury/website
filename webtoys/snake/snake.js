//Set constants
const COLS = 16;
const ROWS = 16;
const GRID_SIZE = 32;
const FOOD_SIZE = GRID_SIZE * 0.75;
const SNAKE_HEAD_SIZE = GRID_SIZE * 0.75;
const SNAKE_SIZE = GRID_SIZE * 0.5;
const SNAKE_COLOR = "#0080ff";
const FOOD_COLOR = "#ff0000";
const BG_COLOR = "#000000";

//Set canvas size and get context
var c = document.getElementById("canv");
c.width = COLS * GRID_SIZE;
c.height = ROWS * GRID_SIZE;
var ctx = c.getContext("2d");

function drawScreen(){
	//Draw background
	ctx.fillStyle = BG_COLOR;
	ctx.fillRect(0, 0, c.width, c.height);

	//Draw food
	if(food){
		ctx.fillStyle = FOOD_COLOR;
		var offset = (GRID_SIZE - FOOD_SIZE) / 2;
		ctx.fillRect(food[0]*GRID_SIZE + offset, food[1]*GRID_SIZE + offset, FOOD_SIZE, FOOD_SIZE);
	}

	//Draw snake
	ctx.fillStyle = SNAKE_COLOR;
	for(i in player.snake){
		if(i == 0){ //i is actually a string for some reason, so `if(i)` wouldn't work
			var offset = (GRID_SIZE - SNAKE_HEAD_SIZE) / 2;
			ctx.fillRect(player.snake[i][0]*GRID_SIZE + offset, player.snake[i][1]*GRID_SIZE + offset, SNAKE_HEAD_SIZE, SNAKE_HEAD_SIZE);
		}else{
			var offset = (GRID_SIZE - SNAKE_SIZE) / 2;
			if(player.snake[i - 1][0] == player.snake[i][0] && player.snake[i - 1][1] == player.snake[i][1] - 1) //Above
				ctx.fillRect(player.snake[i][0]*GRID_SIZE + offset, player.snake[i][1]*GRID_SIZE - offset, SNAKE_SIZE, GRID_SIZE);
			else if(player.snake[i - 1][0] == player.snake[i][0] - 1 && player.snake[i - 1][1] == player.snake[i][1]) //Left
				ctx.fillRect(player.snake[i][0]*GRID_SIZE - offset, player.snake[i][1]*GRID_SIZE + offset, GRID_SIZE, SNAKE_SIZE);
			else if(player.snake[i - 1][0] == player.snake[i][0] && player.snake[i - 1][1] == player.snake[i][1] + 1) //Below
				ctx.fillRect(player.snake[i][0]*GRID_SIZE + offset, player.snake[i][1]*GRID_SIZE + offset, SNAKE_SIZE, GRID_SIZE);
			else if(player.snake[i - 1][0] == player.snake[i][0] + 1 && player.snake[i - 1][1] == player.snake[i][1]) //Right
				ctx.fillRect(player.snake[i][0]*GRID_SIZE + offset, player.snake[i][1]*GRID_SIZE + offset, GRID_SIZE, SNAKE_SIZE);
			else
				ctx.fillRect(player.snake[i][0]*GRID_SIZE + offset, player.snake[i][1]*GRID_SIZE + offset, SNAKE_SIZE, SNAKE_SIZE);
		}
	}

	//Update score
	document.getElementById("score").innerHTML = "Score: " + player.score;
}

function rand(limit){
	return Math.floor(Math.random()*limit); //Random integer from 0 to limit-1
}

var player = {
	snake: [[rand(COLS), rand(ROWS)]],
	direction: rand(4),
	direction: 2,
	movement: [],
	alive: true,
	score: 0
};

var food;
function newFood(){
	if(player.snake.length < COLS * ROWS){
		var validFoodPos; //If the food is currently in a valid position (i.e. one the snake isn't in)
		do{
			validFoodPos = true;
			food = [rand(COLS), rand(ROWS)];
			for(i in player.snake){
				if(food[0] == player.snake[i][0] && food[1] == player.snake[i][1])
					validFoodPos = false;
			}
		}while(!validFoodPos)
	}else{
		food = null;
	}
}

var graceTurn = false;

newFood();
drawScreen();

function update(){
	if(player.alive){
		if(player.movement.length > 0)
			player.direction = player.movement.shift();
		move(player.direction);
	}
	drawScreen();
}

function move(direction){
	var coord = player.snake[0].slice();
	switch(direction){
		case 0: //Up
			coord[1]--;
			break;
		case 1: //Left
			coord[0]--;
			break;
		case 2: //Down
			coord[1]++;
			break;
		case 3: //Right
			coord[0]++;
			break;
	}
	var collidesWall = (coord[0] < 0 || coord[0] >= COLS || coord[1] < 0 || coord[1] >= ROWS);
	var collidesSnake = false;
	for(i in player.snake){
		if(i != player.snake.length - 1 && coord[0] == player.snake[i][0] && coord[1] == player.snake[i][1])
			collidesSnake = true;
	}

	if(collidesWall || collidesSnake){
		if(graceTurn){
			player.snake.unshift(coord);
			player.snake.pop();
			player.alive = false;
		}else{
			graceTurn = true; //If the player is about to die, do nothing for one time step so they have extra time to correct it
		}
	}else{
		graceTurn = false;
		player.snake.unshift(coord);
		if(coord[0] == food[0] && coord[1] == food[1]){
			newFood();
			player.score++;
		}else{
			player.snake.pop();
		}
	}
}

function getKey(event){
	var keyCode = event.which || event.keyCode;
	var direction;
	switch(keyCode){
		case 119: //w
			direction = 0;
			break;
		case 97: //a
			direction = 1;
			break;
		case 115: //s
			direction = 2;
			break;
		case 100: //d
			direction = 3;
			break;
	}
	
	addDirection(direction);
}

function addDirection(direction){
	var lastDirection;
	if(player.movement.length > 0)
		lastDirection = player.movement[player.movement.length - 1];
	else
		lastDirection = player.direction;
	var willRunIntoSelf = direction == (lastDirection + 2) % 4 && player.snake.length > 2;
	//willRunIntoSelf checks if the snake is about to move backwards on top of itself
	if(direction != undefined && !willRunIntoSelf)
		player.movement.push(direction);
}

loop = window.setInterval(update, 200);
